import javax.swing.*;
import java.awt.*;

public class MyWindow {

    public static void main(String args[])
    {
         Runnable init = new Runnable()
         {
             public void run()
             {

                    JFrame myWindow = new JFrame("Hello!");
                    myWindow.setForeground(Color.YELLOW);
                    myWindow.setBackground(Color.YELLOW);
                    myWindow.setSize(400, 300);

                    myWindow.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    myWindow.setLayout(null);

                    myWindow.setVisible(true);

             }

         };
         SwingUtilities.invokeLater(init);
    }


}